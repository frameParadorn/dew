<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(isset($_GET['action']) && $_GET['action']=="add"){
	$id=intval($_GET['id']);
	if(isset($_SESSION['cart'][$id])){
		$_SESSION['cart'][$id]['quantity']++;
	}else{
		$sql_p="SELECT * FROM products WHERE id={$id}";
		$query_p=mysqli_query($con,$sql_p);
		if(mysqli_num_rows($query_p)!=0){
			$row_p=mysqli_fetch_array($query_p);
			$_SESSION['cart'][$row_p['id']]=array("quantity" => 1, "price" => $row_p['productPrice']);
			header('location:index.php');
		}else{
			$message="Product ID is invalid";
		}
	}
}


?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Meta -->
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="">
		<meta name="author" content="">
	    <meta name="keywords" content="MediaCenter, Template, eCommerce">
	    <meta name="robots" content="all">

	    <title>ขายรองเท้าผ้าใบ มือสอง</title>

	    <!-- Bootstrap Core CSS -->
	    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
	    
	    <!-- Customizable CSS -->
	    <link rel="stylesheet" href="assets/css/main.css">
	    <link rel="stylesheet" href="assets/css/green.css">
	    <link rel="stylesheet" href="assets/css/owl.carousel.css">
		<link rel="stylesheet" href="assets/css/owl.transitions.css">
		<!--<link rel="stylesheet" href="assets/css/owl.theme.css">-->
		<link href="assets/css/lightbox.css" rel="stylesheet">
		<link rel="stylesheet" href="assets/css/animate.min.css">
		<link rel="stylesheet" href="assets/css/rateit.css">
		<link rel="stylesheet" href="assets/css/bootstrap-select.min.css">

		<!-- Demo Purpose Only. Should be removed in production -->
		<link rel="stylesheet" href="assets/css/config.css">

		<link href="assets/css/green.css" rel="alternate stylesheet" title="Green color">
		<link href="assets/css/blue.css" rel="alternate stylesheet" title="Blue color">
		<link href="assets/css/red.css" rel="alternate stylesheet" title="Red color">
		<link href="assets/css/orange.css" rel="alternate stylesheet" title="Orange color">
		<link href="assets/css/dark-green.css" rel="alternate stylesheet" title="Darkgreen color">
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">
		<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/images/favicon.ico">

    </head>
    <style>
        #contatti{
        letter-spacing: 2px;
        }
        #contatti a{
        color: #fff;
        text-decoration: none;
        }


        @media (max-width: 575.98px) {

        #contatti .maps iframe{
            width: 100%;
            height: 450px;
        }
        }


        @media (min-width: 576px) {



        #contatti .maps iframe{
            width: 100%;
            height: 450px;
        }
        }

        @media (min-width: 768px) {

        #contatti .maps iframe{
            width: 100%;
            height: 850px;
        }
        }

        @media (min-width: 992px) {

        #contatti .maps iframe{
            width: 100%;
            height: 700px;
        }
        }


        #author a{
        color: #fff;
        text-decoration: none;
            
        }
    </style>
    <body class="cnt-home">
	
		
	
		<!-- ============================================== HEADER ============================================== -->
        <header class="header-style-1">
        <?php include('includes/top-header.php');?>
        <?php include('includes/main-header.php');?>
        <?php include('includes/menu-bar.php');?>
        </header>

        <div class="container mt-5" id="contatti">

            <div class="row" style="height:550px; margin-top: 50px;">
                <div class="col-md-6 maps" >
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3879.197403346438!2d100.75251421485333!3d13.523473906186965!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e298e70af2dbb3%3A0xd717f2816d3edc3c!2z4Lih4Lir4Liy4Lin4Li04LiX4Lii4Liy4Lil4Lix4Lii4Lij4Liy4LiK4Lig4Lix4LiP4LiY4LiZ4Lia4Li44Lij4Li1IOC4quC4oeC4uOC4l-C4o-C4m-C4o-C4suC4geC4suC4ow!5e0!3m2!1sth!2sth!4v1573058710727!5m2!1sth!2sth" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                </div>
                <div class="col-md-6">
                    <h2 class="text-uppercase mt-3 font-weight-bold text-white">Contact</h2>
                    <?php
                    
                    if(isset($_GET["save"])) {
                        if($_GET["save"] == "true") {
                            echo '
                                <div class="alert alert-success">
                                    ทางเราข้อมูลแล้ว จะติดต่อกลับโดยเร็วที่สุด ขอบคุณค่ะ
                                </div>
                            ';
                        }else {
                            echo '
                                <div class="alert alert-danger">
                                    ระบบผิดดพลาด กรุณาติดต่อใหม่อีกครั้งภายหลังค่ะ
                                </div>
                            ';
                        }
                    }
                    

                    
                    ?>
                    <form action="contact-save.php">
                        <div class="row">
                            <div class="col-lg-6">
                            <div class="form-group">
                                <input type="text" class="form-control mt-2" placeholder="ชื่อ" name="name" required>
                            </div>
                            </div>
                            <div class="col-lg-6">
                            <div class="form-group">
                                <input type="text" class="form-control mt-2" placeholder="นามสกุล" name="lastname" required>
                            </div>
                            </div>
                            <div class="col-lg-6">
                            <div class="form-group">
                                <input type="email" class="form-control mt-2" placeholder="อีเมล์" name="email" required>
                            </div>
                            </div>
                            <div class="col-lg-6">
                            <div class="form-group">
                                <input type="number" class="form-control mt-2" placeholder="โทร." name="tel" required>
                            </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <textarea class="form-control" id="exampleFormControlTextarea1" name="address" placeholder="ที่อยู่" rows="3" required></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="col-12">
                                        <button class="btn btn-success btn-lg" type="submit">ส่ง</button>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </form>
                </div>

            </div>
        </div>


        








        <?php include('includes/footer.php');?>
	
	<script src="assets/js/jquery-1.11.1.min.js"></script>
	
	<script src="assets/js/bootstrap.min.js"></script>
	
	<script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
	<script src="assets/js/owl.carousel.min.js"></script>
	
	<script src="assets/js/echo.min.js"></script>
	<script src="assets/js/jquery.easing-1.3.min.js"></script>
	<script src="assets/js/bootstrap-slider.min.js"></script>
    <script src="assets/js/jquery.rateit.min.js"></script>
    <script type="text/javascript" src="assets/js/lightbox.min.js"></script>
    <script src="assets/js/bootstrap-select.min.js"></script>
    <script src="assets/js/wow.min.js"></script>
	<script src="assets/js/scripts.js"></script>

	<!-- For demo purposes – can be removed on production -->
	
	<script src="switchstylesheet/switchstylesheet.js"></script>
	
	<script>
		$(document).ready(function(){ 
			$(".changecolor").switchstylesheet( { seperator:"color"} );
			$('.show-theme-options').click(function(){
				$(this).parent().toggleClass('open');
				return false;
			});
		});

		$(window).bind("load", function() {
		   $('.show-theme-options').delay(2000).trigger('click');
		});
	</script>
	<!-- For demo purposes – can be removed on production : End -->

	

</body>
</html>